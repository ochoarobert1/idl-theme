<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main">
    <div class="row">
        <?php $files = rwmb_meta( 'rw_page_parallax', 'type=image&size=full' ); // Prior to 4.8.0
        if ( !empty( $files ) ) {
            foreach ( $files as $file ) {
                $url = $file['url'];
            }
        } ?>
        <section class="big-hero col-md-12 no-paddingl no-paddingr" <?php if (!empty($files)){ echo 'style="background: url(' . $url . ');"'; } ?> >
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <?php $pickcolor = get_post_meta(get_the_ID(), 'rw_tcolor', true); ?>
                        <?php if ($pickcolor == ''){ $color = '#FFF'; } else { $color = $pickcolor; } ?>
                        <h1 style="color: <?php echo $color; ?>"><?php the_title(); ?></h1>
                        <p style="color: <?php echo $color; ?>"><?php echo get_post_meta(get_the_ID(), 'rw_p_title', true);?></p>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <section class="page-content col-md-12 no-paddingl no-paddingr">
                    <h1><?php the_title(); ?></h1>
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <?php the_content(); ?>
                        <?php comments_template( '', true ); // Remove if you don't want comments ?>
                        <br class="clear">
                        <?php edit_post_link(); ?>
                    </article>
                </section>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
