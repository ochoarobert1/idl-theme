<?php get_header(); ?>
<main class="container-fluid" role="main">
    <div class="row">
        <?php $the_slug = 'news';
        $args = array(
            'name'        => $the_slug,
            'post_type'   => 'page',
            'post_status' => 'publish',
            'numberposts' => 1
        );
        $my_posts = get_posts($args); ?>
        <?php $files = rwmb_meta( 'rw_page_parallax', 'type=image&size=full', $my_posts[0]->ID ); // Prior to 4.8.0
        if ( !empty( $files ) ) {
            foreach ( $files as $file ) {
                $url = $file['url'];
            }
        } ?>
        <section class="big-hero col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr" <?php if (!empty($files)){ echo 'style="background: url(' . $url . ');"'; } ?> >
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <h1>News</h1>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <section class="page-content col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php $defaultatts = array('class' => 'img-responsive'); ?>
                        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                        <article id="post-<?php the_ID(); ?>" class="archive-item col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr <?php echo join(' ', get_post_class()); ?>">
                            <picture class="col-md-5 col-sm-6 col-xs-6">
                                <?php if ( has_post_thumbnail()) : ?>
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                    <?php the_post_thumbnail('blog_img', $defaultatts); ?>
                                </a>
                                <?php else : ?>
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/no-img.jpg" alt="No img" class="img-responsive" />
                                </a>
                                <?php endif; ?>
                            </picture>
                            <div class="col-md-7 col-sm-6 col-xs-6">
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h2><?php the_title(); ?></h2></a>
                                <span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
                                <p><?php the_excerpt(); ?></p>
                            </div>
                            <div class="clearfix"></div>
                            <hr>
                        </article>
                        <?php endwhile; ?>
						<?php echo do_shortcode('[wp-rss-aggregator]'); ?>
                        <div class="pagination col-md-12 col-sm-12 col-xs-12">
                            <?php if(function_exists('wp_paginate')) { wp_paginate(); } else { posts_nav_link(); } ?>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-12">
                        <?php get_sidebar(); ?>
                    </div>
                    <?php else: ?>
                    <article>
                        <h2>Disculpe, su busqueda no arrojo ningun resultado</h2>
                        <h3>Haga click <a href="<?php echo home_url('/'); ?>">aqui</a> para volver al inicio</h3>
                    </article>
                    <?php endif; ?>
                </section>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
