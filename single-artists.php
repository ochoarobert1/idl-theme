<?php get_header(); ?>
<?php the_post(); ?>
<?php $defaultatts = array('class' => 'img-responsive'); ?>
<main class="container-fluid" role="main">
    <div class="row">
        <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
        <section class="big-hero col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr" style="background: url(<?php echo $url; ?>); background-size: cover;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <section class="page-content col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                    <div class="col-md-9 col-sm-12 col-xs-12">
                        <?php $video = get_post_meta(get_the_ID(), 'rw_yvideo', true); ?>
                        <?php if ($video != '') { ?>
                        <?php $link = sum_video_parser($video, true);  ?>
                        <div class="artist-video embed-responsive embed-responsive-16by9">
                            <?php echo $link['embed']; ?>
                        </div>
                        <?php } ?>

                        <?php the_content(); ?>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="logo-band col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                            <?php $logo = rwmb_meta('rw_logo'); ?>
                            <?php foreach ($logo as $item) { ?>
                            <img src="<?php echo $item[full_url]; ?>" alt="" class="img-responsive" />
                            <?php } ?>
                        </div>
                        <div class="social-band col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                            <a href="<?php echo get_post_meta(get_the_ID(), 'rw_social_fb', true); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="<?php echo get_post_meta(get_the_ID(), 'rw_social_tw', true); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="<?php echo get_post_meta(get_the_ID(), 'rw_social_ig', true); ?>" target="_blank"><i class="fa fa-instagram"></i></a>
                            <a href="<?php echo get_post_meta(get_the_ID(), 'rw_social_sc', true); ?>" target="_blank"><i class="fa fa-soundcloud"></i></a>
                            <a href="<?php echo get_post_meta(get_the_ID(), 'rw_social_yt', true); ?>" target="_blank"><i class="fa fa-youtube"></i></a>
                        </div>
                        <div class="lineup-band col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                            <?php $lineup = rwmb_meta('rw_lineup'); ?>
                            <?php $errors = array_filter($lineup); ?>
                            <?php if (!empty($errors)) { ?>
                            <h3>Line-up</h3>
                            <?php foreach ($lineup as $item) { ?>
                            <h4><?php echo $item; ?></h4>
                            <?php } } ?>
                        </div>
                        <div class="lineup-band col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                            <?php $lineup2 = rwmb_meta('rw_discography'); ?>
                            <?php $errors = array_filter($lineup2); ?>
                            <?php if (!empty($errors)) { ?>
                            <h3>Discography</h3>
                            <?php foreach ($lineup2 as $item) { ?>
                            <h4><?php echo $item; ?></h4>
                            <?php } } ?>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
