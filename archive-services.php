<?php get_header(); ?>
<?php $defaultatts = array('class' => 'img-responsive'); ?>
<main class="container-fluid" role="main">
    <div class="row">
        <section class="big-hero big-hero-archive col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <h1>Services</h1>
                        <?php $multivalue = get_option('_ccp_services_text'); ?>
                        <?php if (!$multivalue == false ) { ?>
                        <p><?php echo $multivalue; ?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <div class="container-fluid">
            <div class="row">
                <section class="page-content col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                    <?php $i = 1; ?>
                    <?php $y = 1; ?>
                    <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                    <a href="<?php the_permalink(); ?>" class="front-services-item-link">
                       
                       <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                        <div class="front-service-item col-md-3 col-sm-6 col-xs-12 no-paddingl no-paddingr wow fadeIn delay-<?php echo $i; ?>" style="background: url(<?php echo $url; ?>)">
                            <div class="front-service-item-mask"></div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php the_title(); ?>
                            </div>
                        </div>
                    </a>
                    <?php $i++; $y++; if ($i == 4) { $i = 1; } endwhile; ?>
                    <?php endif; ?>
                    <?php wp_reset_query();?>
                </section>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
