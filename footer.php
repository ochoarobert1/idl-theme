<footer class="container-fluid" role="contentinfo">
    <div class="row">
        <div class="the-footer col-md-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="footer-info col-md-12">
                        <p>&copy; 2016 - IDL Entertaiment - All Rights Reserved.</p>
                        <div class="footer-icons col-md-12">
                            <a href="https://www.facebook.com/IDLEntertainment/" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/fb.png" alt=""></a>
                            <a href="https://twitter.com/idlent" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/tw.png" alt=""></a>
                            <a href="https://www.instagram.com/idlentertainment/" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ig.png" alt=""></a>
                            <a href="https://soundcloud.com/idlentertainment" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/sc.png" alt=""></a>
                            <a href="https://youtube.com/idlentertainment" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/yt.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>
</body>
</html>
