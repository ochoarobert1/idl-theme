<?php get_header(); ?>
<?php $defaultatts = array('class' => 'img-responsive'); ?>
<main class="container-fluid" role="main">
    <div class="row">
        <section class="big-hero big-hero-archive col-md-12 col-sm-12 col-xs-12  no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 ">
                        <h1>Resources</h1>
                        <?php $multivalue = get_option('_cpp_artists_text'); ?>
                        <?php if (!$multivalue == false ) { ?>
                        <p><?php echo $multivalue; ?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <section class="page-content col-md-12 col-sm-12 col-xs-12  no-paddingl no-paddingr">
                    <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                    <div class="artist-archive-item col-md-4 col-sm-4 col-xs-12 ">
                        <div class="artists-item wow fadeIn col-md-12 col-sm-12 col-xs-12 ">
                            <?php $url = get_post_meta(get_the_ID(), 'rw_resource_url', true); ?>
                            <a href="<?php echo $url; ?>" target="_blank">
                                <div class="artists-item-img col-md-12 col-sm-12 col-xs-12  no-paddingl no-paddingr">
                                    <?php the_post_thumbnail('blog_img', $defaultatts); ?>
                                </div>
                            </a>
                            <div class="artists-item-title col-md-12 col-sm-12 col-xs-12  no-paddingl no-paddingr">
                                <a href="<?php echo $url; ?>"><?php the_title(); ?></a>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; endif;?>
                </section>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>


<form enctype="application/x-www-form-urlencoded" action="https://mjayarob.ip-zone.com/ccm/subscribe/index/form/" method="post">
    <dl class="zend_form">
        <input type="text" name="name" id="name" value="" placeholder="Nombre">
        <input type="text" name="email" id="email" value="" placeholder="Email">
        <input type="hidden" name="groups[]" id="groups-1" value="1"/>
        <input type="hidden" name="anotheremail" id="anotheremail" value="" style="position: absolute; left: -5000px" tabindex="-1"></dd>
        <dt id="submit-label"> </dt>
        <dd id="submit-element">
            <input type="submit" name="submit" id="submit" value="Suscribir"></dd>
    </dl>
</form>
<p> <a href="http://mailrelay.com" target="_blank">Suscribirte</a> by Mailrelay </p>