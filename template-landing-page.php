<?php /**
 * Template Name: Landing Template
 *
 * @package WordPress
 * @subpackage idl_theme
 * @since IDL Entertaiment theme 1.0
 */ ?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main">
    <div class="row">
        <?php $files = rwmb_meta( 'rw_page_parallax', 'type=image&size=full' ); // Prior to 4.8.0
        if ( !empty( $files ) ) {
            foreach ( $files as $file ) {
                $url = $file['url'];
            }
        } ?>
        <section class="landing-container col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr" <?php if (!empty($files)){ echo 'style="background: url(' . $url . ');"'; } ?>>
            <div class="container">
                <div class="row">
                   <div class="landing-title col-md-12 col-sm-12 col-xs-12">
                       <h1><?php the_title(); ?></h1>
                   </div>
                    <section class="page-content landing-content col-md-8 col-sm-8 col-xs-12">
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <?php the_content(); ?>
                        </article>
                    </section>
                    <aside class="sidebar-landing col-md-4 col-sm-4 col-xs-12">
                        <div id="sticker-landing" class="col-md-12 col-sm-12 col-xs-12">
                            <?php $contact_shortcode = get_post_meta(get_the_ID(), 'rw_wpcf7_shortcode', true); ?>
                            <?php echo do_shortcode("$contact_shortcode"); ?>
                        </div>
                    </aside>
                </div>
            </div>
        </section>

    </div>
</main>
<?php get_footer(); ?>
