<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main">
    <div class="row">
        <?php $files = rwmb_meta( 'rw_service_parallax', 'type=image&size=full' ); // Prior to 4.8.0
        if ( !empty( $files ) ) {
            foreach ( $files as $file ) {
                $url = $file['url'];
            }
        } ?>
        <section class="big-hero col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr" <?php if (!empty($files)){ echo 'style="background: url(' . $url . ');"'; } ?> >
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?php $pickcolor = get_post_meta(get_the_ID(), 'rw_tcolor', true); ?>
                        <?php if ($pickcolor == ''){ $color = '#FFF'; } else { $color = $pickcolor; } ?>
                        <h1 style="color: <?php echo $color; ?>"><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <section class="page-content col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                    <div class="col-md-8 col-sm-12 col-xs-12">
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <?php the_content(); ?>
                        </article>
                    </div>
                    <div class="col-md-4 visible-md visible-lg">
                        <?php $images = get_post_meta(get_the_ID(), 'rw_services_sidebar', true); ?>
                        <?php echo $images; ?>
                    </div>
                    <div class="clearfix"></div>

                </section>
            </div>
        </div>
        <?php $args = array('post_type' => 'services'); ?>
        <?php query_posts($args); ?>
        <?php $i = 1; ?>
        <?php $y = 1; ?>
        <?php while (have_posts()) : the_post(); ?>
        <a href="<?php the_permalink(); ?>" class="front-service-item-link">
            <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
            <div class="front-service-item col-md-3 col-sm-6 col-xs-12 no-paddingl no-paddingr wow fadeIn delay-<?php echo $i; ?>" style="background: url(<?php echo $url; ?>)">
                <div class="front-service-item-mask"></div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <?php the_title(); ?>
                </div>
            </div>
        </a>
        <?php $i++; $y++; if ($i == 4) { $i = 1; }endwhile; ?>
        <?php wp_reset_query();?>
    </div>
</main>
<?php get_footer(); ?>
