<?php get_header(); ?>
<?php $defaultatts = array('class' => 'img-responsive'); ?>
<main class="container-fluid" role="main">
    <div class="row">
        <section class="big-hero big-hero-noparallax col-md-12 col-sm-12 col-xs-12  no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 ">
                        <h1>Artists</h1>
                        <?php $multivalue = get_option('_cpp_artists_text'); ?>
                        <?php if (!$multivalue == false ) { ?>
                        <p><?php echo $multivalue; ?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <section class="page-content col-md-12 col-sm-12 col-xs-12  no-paddingl no-paddingr">
                    <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                    <div class="artist-archive-item col-md-4 col-sm-4 col-xs-12 ">
                        <div class="artists-item reswow fadeIn col-md-12 col-sm-12 col-xs-12 ">
                            <a href="<?php the_permalink(); ?>">
                                <div class="artists-item-img col-md-12 col-sm-12 col-xs-12  no-paddingl no-paddingr">
                                    <?php the_post_thumbnail('blog_img', $defaultatts); ?>
                                </div>
                            </a>
                            <div class="artists-item-title col-md-12 col-sm-12 col-xs-12  no-paddingl no-paddingr">
                                <?php $url = get_post_meta(get_the_ID(), 'rw_resource_url', true); ?>
                                <a href="<?php echo $url; ?>"><?php the_title(); ?></a>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; endif;?>
                </section>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
