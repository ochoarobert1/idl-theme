<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main">
    <div class="row">
        <?php $files = rwmb_meta( 'rw_post_parallax', 'type=image&size=full' ); // Prior to 4.8.0
        if ( !empty( $files ) ) {
            foreach ( $files as $file ) {
                $url = $file['url'];
            }
        } ?>
        <section class="big-hero col-md-12 no-paddingl no-paddingr" <?php if (!empty($files)){ echo 'style="background: url(' . $url . ');"'; } ?> >
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <?php $pickcolor = get_post_meta(get_the_ID(), 'rw_tcolor', true); ?>
                        <?php if ($pickcolor == ''){ $color = '#FFF'; } else { $color = $pickcolor; } ?>
                        <h1 style="color: <?php echo $color; ?>"><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <section class="page-content col-md-12 no-paddingl no-paddingr">
                    <div class="col-md-8">
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                            <?php the_content(); ?>
                        </article>
                    </div>
                    <div class="col-md-4">
                       <?php get_sidebar(); ?>
                    </div>
                    <div class="clearfix"></div>

                </section>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
