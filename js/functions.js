var openMenu = false;
$(document).ready(function () {
    "use strict";
    $("#sticker").sticky({topSpacing: 0});
    $('#sticker-landing').sticky({
        topSpacing: 120,
        bottomSpacing: 160
    });
    $("body").niceScroll({
        cursorcolor: '#BE1B20',
        cursorborder: '0px',
        cursorwidth: '10px',
        cursorborderradius: '0px',
        background: '#000000',
        hwacceleration: false,
        scrollspeed: 60,
        mousescrollstep: 85,
        smoothscroll: true,
        autohidemode: false,
        zindex: '999'
    });
    $('.canvas-img-container').removeClass('canvas-img-container-left');
    setTimeout(function(){

        $('.giphy-overlay').removeClass('giphy-show');
        $('.giphy-overlay').addClass('giphy-hide');
        $('.canvas-img-container').addClass('canvas-img-container-no-left');
    }, 1000);
});

$('#menu-open').click(function () {
    if (openMenu === false){
        $('.special-menu-container').removeClass('menu-hide');
        $('.special-menu-container').addClass('menu-show');
        openMenu = true;
    } else {
        $('.special-menu-container').removeClass('menu-show');
        $('.special-menu-container').addClass('menu-hide');
        openMenu = false;
    }
});

$('#menu-close').click(function () {
    if (openMenu === false){
        $('.special-menu-container').removeClass('menu-hide');
        $('.special-menu-container').addClass('menu-show');
        openMenu = true;
    } else {
        $('.special-menu-container').removeClass('menu-show');
        $('.special-menu-container').addClass('menu-hide');
        openMenu = false;
    }
});



var wow = new WOW(
    {
        boxClass:     'wow',      // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset:       0,          // distance to the element when triggering the animation (default is 0)
        mobile:       true,       // trigger animations on mobile devices (default is true)
        live:         true,       // act on asynchronously loaded content (default is true)
        callback:     function(box) {
            // the callback is fired every time an animation is started
            // the argument that is passed in is the DOM node being animated
        },
        scrollContainer: null // optional scroll container selector, otherwise use window
    }
);
wow.init();

smoothScroll.init({
    selector: '[data-scroll]', // Selector for links (must be a valid CSS selector)
    selectorHeader: '[data-scroll-header]', // Selector for fixed headers (must be a valid CSS selector)
    speed: 500, // Integer. How fast to complete the scroll in milliseconds
    easing: 'easeInOutCubic', // Easing pattern to use
    offset: 110, // Integer. How far to offset the scrolling anchor location in pixels
    updateURL: true, // Boolean. If true, update the URL hash on scroll
    callback: function ( anchor, toggle ) {} // Function to run after scrolling
});

$('.canvas-container').click(function() {
    $('.fillWidth')[0].pause();
    $('.video-overlay').removeClass('overlay-hide');
    $('.video-overlay').addClass('overlay-show');
    $('.fullVideo')[0].play();
});
$('#closevideo').click(function (){
    $('.fullVideo')[0].pause();
    $('.video-overlay').removeClass('overlay-show');
    $('.video-overlay').addClass('overlay-hide');
    $('.fillWidth')[0].play();
});
