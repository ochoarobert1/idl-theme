<?php

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER CSS
-------------------------------------------------------------- */

function idl_load_css() {
    if (!is_admin()){
        $version_remove = NULL;
        if ($_SERVER['REMOTE_ADDR'] == '::1') {

            /*- BOOTSTRAP CORE ON LOCAL -*/
            wp_register_style('bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', false, '3.3.6', 'all');
            wp_enqueue_style('bootstrap-css');

            /*- BOOTSTRAP THEME ON LOCAL -*/
            wp_register_style('bootstrap-theme', get_template_directory_uri() . '/css/bootstrap-theme.min.css', array('bootstrap-css'), '3.3.6', 'all');
            wp_enqueue_style('bootstrap-theme');

            /*- FONT AWESOME ON LOCAL -*/
            wp_register_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', false, '4.5.0', 'all');
            wp_enqueue_style('font-awesome');

            /*- VALIDATION ENGINE ON LOCAL -*/
            //wp_register_style('validation-engine', get_template_directory_uri() . '/css/validationEngine.min.css', false, '2.6.4', 'all');
            //wp_enqueue_style('validation-engine');

            /*- ANIMATE CSS ON LOCAL -*/
            wp_register_style('animate-css', get_template_directory_uri() . '/css/animate.css', false, '3.5.0', 'all');
            wp_enqueue_style('animate-css');

            /*- FLICKITY ON LOCAL -*/
            //wp_register_style('flickity-css', get_template_directory_uri() . '/css/flickity.min.css', false, '1.1.1', 'all');
            //wp_enqueue_style('flickity-css');

        } else {

            /*- BOOTSTRAP CORE -*/
            wp_register_style('bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css', false, '3.3.6', 'all');
            wp_enqueue_style('bootstrap-css');

            /*- BOOTSTRAP THEME -*/
            wp_register_style('bootstrap-theme', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css', array('bootstrap-css'), '3.3.6', 'all');
            wp_enqueue_style('bootstrap-theme');

            /*- FONT AWESOME -*/
            wp_register_style('font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css', false, '4.5.0', 'all');
            wp_enqueue_style('font-awesome');

            /*- VALIDATION ENGINE -*/
            //wp_register_style('validation-engine', 'https://cdn.jsdelivr.net/jquery.validationengine/2.6.4/css/validationEngine.jquery.css', false, '2.6.4', 'all');
            //wp_enqueue_style('validation-engine');

            /*- ANIMATE CSS -*/
            wp_register_style('animate-css', 'https://cdn.jsdelivr.net/animatecss/3.4.0/animate.min.css', false, '3.5.0', 'all');
            wp_enqueue_style('animate-css');

            /*- FLICKITY -*/
            //wp_register_style('flickity-css', 'https://cdn.jsdelivr.net/flickity/1.1.2/flickity.min.css', false, '1.1.2', 'all');
            //wp_enqueue_style('flickity-css');
        }

        /*- GOOGLE FONTS -*/
        wp_register_style('google-fonts', 'https://fonts.googleapis.com/css?family=Oswald:300,400,700', false, $version_remove, 'all');
        wp_enqueue_style('google-fonts');

        /*- MAIN STYLE -*/
        wp_register_style('main-style', get_template_directory_uri() . '/css/idl-style.css', false, $version_remove, 'all');
        wp_enqueue_style('main-style');

        /*- MAIN MEDIAQUERIES -*/
        wp_register_style('main-mediaqueries', get_template_directory_uri() . '/css/idl-mediaqueries.css', array('main-style'), $version_remove, 'all');
        wp_enqueue_style('main-mediaqueries');
    }
}

add_action('init', 'idl_load_css');

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER JS
-------------------------------------------------------------- */

if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", '');
function my_jquery_enqueue() {
    wp_deregister_script('jquery');
    if ($_SERVER['REMOTE_ADDR'] == '::1') {
        /*- JQUERY ON LOCAL  -*/
        wp_register_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', false, '1.12.0', true);
    } else {
        /*- JQUERY ON WEB  -*/
        wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js', false, '1.12.0', true);
    }
    wp_enqueue_script('jquery');
}


function idl_load_js() {
    if (!is_admin()){
        $version_remove = NULL;
        if ($_SERVER['REMOTE_ADDR'] == '::1') {
            /*- MODERNIZR ON LOCAL  -*/
            wp_register_script('modernizr', get_template_directory_uri() . '/js/modernizr.min.js', array('jquery'), '2.8.3', true);
            wp_enqueue_script('modernizr');

            /*- BOOTSTRAP ON LOCAL  -*/
            wp_register_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.3.6', true);
            wp_enqueue_script('bootstrap');

            /*- JQUERY STICKY ON LOCAL  -*/
            wp_register_script('sticky', get_template_directory_uri() . '/js/jquery.sticky.js', array('jquery'), '1.0.3', true);
            wp_enqueue_script('sticky');

            /*- VALIDATION ENGINE ON LOCAL  -*/
            //wp_register_script('validation', get_template_directory_uri() . '/js/jquery.validationEngine.min.js', array('jquery'), '2.6.4', true);
            //wp_enqueue_script('validation');

            /*- JQUERY NICESCROLL ON LOCAL  -*/
            wp_register_script('nicescroll', get_template_directory_uri() . '/js/jquery.nicescroll.min.js', array('jquery'), '3.6.6', true);
            wp_enqueue_script('nicescroll');

            /*- LETTERING  -*/
            //wp_register_script('lettering', get_template_directory_uri() . '/js/jquery.lettering.js', array('jquery'), '0.7.0', true);
            //wp_enqueue_script('lettering');

            /*- IMAGESLOADED ON LOCAL  -*/
            //wp_register_script('imagesloaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.js', array('jquery'), '4.1.0', true);
            //wp_enqueue_script('imagesloaded');

            /*- SMOOTH SCROLL -*/
            wp_register_script('smooth-scroll', get_template_directory_uri() . '/js/smooth-scroll.js', array('jquery'), '7.1.1', true);
            wp_enqueue_script('smooth-scroll');

            /*- ISOTOPE ON LOCAL  -*/
            //wp_register_script('isotope', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array('jquery'), '2.2.2', true);
            //wp_enqueue_script('isotope');

            /*- FLICKITY ON LOCAL  -*/
            //wp_register_script('flickity', get_template_directory_uri() . '/js/flickity.pkgd.min.js', array('jquery'), '1.1.1', true);
            //wp_enqueue_script('flickity');

            /*- MASONRY ON LOCAL  -*/
            //wp_register_script('masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array('jquery'), '4.4.0', true);
            //wp_enqueue_script('masonry');

        } else {


            /*- MODERNIZR -*/
            wp_register_script('modernizr', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', array('jquery'), '2.8.3', true);
            wp_enqueue_script('modernizr');

            /*- BOOTSTRAP -*/
            wp_register_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js', array('jquery'), '3.3.6', true);
            wp_enqueue_script('bootstrap');

            /*- JQUERY STICKY - VERSION 1.0.3 NOT IN CDN -*/
            wp_register_script('sticky', 'https://cdn.jsdelivr.net/jquery.sticky/1.0.1/jquery.sticky.min.js', array('jquery'), '1.0.1', true);
            wp_enqueue_script('sticky');

            /*- VALIDATION ENGINE -*/
            //wp_register_script('validation', 'https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js', array('jquery'), '2.6.4', true);
            //wp_enqueue_script('validation');

            /*- JQUERY NICESCROLL -*/
            wp_register_script('nicescroll', 'https://cdn.jsdelivr.net/jquery.nicescroll/3.6.6/jquery.nicescroll.min.js', array('jquery'), '3.6.6', true);
            wp_enqueue_script('nicescroll');

            /*- LETTERING  -*/
            //wp_register_script('lettering', 'https://cdnjs.cloudflare.com/ajax/libs/lettering.js/0.7.0/jquery.lettering.min.js', array('jquery'), '0.7.0', true);
            //wp_enqueue_script('lettering');

            /*- SMOOTH SCROLL -*/
            wp_register_script('smooth-scroll', 'https://cdnjs.cloudflare.com/ajax/libs/smooth-scroll/7.1.1/js/smooth-scroll.min.js', array('jquery'), '7.1.1', true);
            wp_enqueue_script('smooth-scroll');

            /*- IMAGESLOADED -*/
            //wp_register_script('imagesloaded', 'https://cdn.jsdelivr.net/imagesloaded/4.1.0/imagesloaded.pkgd.min.js', array('jquery'), '4.1.0', true);
            //wp_enqueue_script('imagesloaded');


            /*- ISOTOPE -*/
            //wp_register_script('isotope', 'https://cdn.jsdelivr.net/isotope/2.2.2/isotope.pkgd.min.js', array('jquery'), '2.2.2', true);
            //wp_enqueue_script('isotope');

            /*- FLICKITY -*/
            //wp_register_script('flickity', 'https://cdn.jsdelivr.net/flickity/1.1.2/flickity.pkgd.min.js', array('jquery'), '1.1.2', true);
            //wp_enqueue_script('flickity');

            /*- MASONRY -*/
            //wp_register_script('masonry', 'https://cdn.jsdelivr.net/masonry/4.0.0/masonry.pkgd.min.js', array('jquery'), '4.4.0', true);
            //wp_enqueue_script('masonry');

        }

        /*- WOW JS -*/
        wp_register_script('wow-js', get_template_directory_uri() . '/js/wow.min.js', array('jquery'), $version_remove, true);
        wp_enqueue_script('wow-js');

        /*- VALIDATION ENGINE LOCALE -*/
        //wp_register_script('validation-es', get_template_directory_uri() . '/js/jquery.validationEngine-es.min.js', array('jquery', 'validation'), $version_remove, true);
        //wp_enqueue_script('validation-es');


        /*- MAIN FUNCTIONS -*/
        wp_register_script('main-functions', get_template_directory_uri() . '/js/functions.js', array('jquery'), $version_remove, true);
        wp_enqueue_script('main-functions');
    }
}

add_action('init', 'idl_load_js');

/* --------------------------------------------------------------
    ADD THEME SUPPORT
-------------------------------------------------------------- */

load_theme_textdomain('idl', get_template_directory() . '/languages');
add_theme_support('post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio' ));
add_theme_support('post-thumbnails');
add_theme_support('automatic-feed-links');
add_theme_support('title-tag');
add_theme_support('menus');

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */
function idl_add_editor_styles() {
    add_editor_style( get_stylesheet_directory_uri() . '/css/editor-styles.css' );
}
add_action( 'admin_init', 'idl_add_editor_styles' );

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */

register_nav_menus( array(
    'header_menu' => __( 'Menu Header Principal', 'idl' ),
    'footer_menu' => __( 'Menu Footer', 'idl' ),
) );

/* --------------------------------------------------------------
    ADD DYNAMIC SIDEBAR SUPPORT
-------------------------------------------------------------- */

add_action( 'widgets_init', 'idl_widgets_init' );
function idl_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'idl' ),
        'id' => 'main_sidebar',
        'description' => __( 'Widgets seran vistos en posts y pages', 'idl' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name' => __( 'Shop Sidebar', 'idl' ),
        'id' => 'shop_sidebar',
        'description' => __( 'Widgets seran vistos en Tienda y Categorias de Producto', 'idl' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}

/* --------------------------------------------------------------
    CUSTOM ADMIN LOGIN
-------------------------------------------------------------- */

add_action('login_head', 'custom_login_logo');
function custom_login_logo() {
    echo '
    <style type="text/css">
        body{
            background-color: #5A5A5A !important;
            background-image:url(' . get_template_directory_uri() . '/images/login-bg.jpg);
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
        }
        h1 {
            background-image:url(' . get_template_directory_uri() . '/images/logo.jpg) !important;
            background-repeat: no-repeat;
            background-position: center;
            background-size: contain !important;
        }
        a { background-image:none !important;}
        .login form{-webkit-border-radius: 5px; border-radius: 5px; background-color: rgba(255,255,255,0.5);}
        .login label{color: black; font-weight: 500;}
    </style>
    ';
}

if (! function_exists('dashboard_footer') ){
    function dashboard_footer() {
        _e( '<span id="footer-thankyou">Gracias por crear con <a href="http://wordpress.org/" >WordPress.</a> - Tema desarrollado por <a href="http://robertochoa.com.ve/" >Robert Ochoa</a></span>', 'idl' );
    }
}
add_filter('admin_footer_text', 'dashboard_footer');

/* --------------------------------------------------------------
    ADD CUSTOM METABOX
-------------------------------------------------------------- */

add_filter( 'rwmb_meta_boxes', 'idl_metabox' );

function idl_metabox( $meta_boxes )
{
    $prefix = 'rw_';

    $meta_boxes[] = array(
        'title'      => __( 'Extra Info', 'idl' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => __( 'Extra Line', 'idl' ),
                'desc'  => 'Extra Line, under the page title',
                'id'    => $prefix . 'p_title',
                'type'  => 'text',
            ),
            array(
                'name'  => __( 'Big Image Parallax', 'idl' ),
                'desc'  => 'Image bigger for background',
                'id'    => $prefix . 'page_parallax',
                'type'  => 'image_advanced',
            ),
            array(
                'name'  => __( 'Info For Landings', 'idl' ),
                'desc'  => 'Contact Form 7 Shortcode',
                'id'    => $prefix . 'wpcf7_shortcode',
                'type'  => 'text',
            ),
        )
    );
    
    $meta_boxes[] = array(
        'title'      => __( 'Extra Info', 'idl' ),
        'post_types' => array( 'post' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => __( 'Stick this post to HomePage', 'idl' ),
                'desc'  => 'Stick this post to HomePage',
                'id'    => $prefix . 'sticker',
                'type'  => 'checkbox',
            ),
            array(
                'name'  => __( 'Big Image Parallax', 'idl' ),
                'desc'  => 'Image bigger for background',
                'id'    => $prefix . 'post_parallax',
                'type'  => 'image_advanced',
            ),
        )
    );

    $meta_boxes[] = array(
        'title'    => 'Extra Info:',
        'pages'    => array( 'artists' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id'   => 'custom_html1',
                // Field name: usually not used
                // 'name' => __( 'Custom HTML', 'your-prefix' ),
                'type' => 'custom_html',
                // HTML content
                'std'  => '<div class="warning"><h4>Current Line-Up</h4></div>',
                // Callback function to show custom HTML
                // 'callback' => 'display_warning',
            ),
            array(
                'name' => 'Add Line-Up',
                'id'   => $prefix . 'lineup',
                'desc' =>  __( "The band's current line-up <br /> E.G.: John Dole - Guitars", "idl" ),
                'clone' => true,
                'type' => 'text',
            ),
            array(
                'id'   => 'custom_html2',
                // Field name: usually not used
                // 'name' => __( 'Custom HTML', 'your-prefix' ),
                'type' => 'custom_html',
                // HTML content
                'std'  => '<div class="warning"><h4>Current Discography</h4></div>',
                // Callback function to show custom HTML
                // 'callback' => 'display_warning',
            ),
            array(
                'name' => 'Add Discography',
                'id'   => $prefix . 'discography',
                'desc' =>  __( "The band's current discography <br /> E.G.: Album Name - (Year of Release)", "idl" ),
                'clone' => true,
                'type' => 'text',
            ),
            array(
                'id'   => 'custom_html3',
                // Field name: usually not used
                // 'name' => __( 'Custom HTML', 'your-prefix' ),
                'type' => 'custom_html',
                // HTML content
                'std'  => '<div class="warning"><h4>Social Networks</h4></div>',
                // Callback function to show custom HTML
                // 'callback' => 'display_warning',
            ),
            array(
                'name' => 'Facebook Page',
                'id'   => $prefix . 'social_fb',
                'desc' =>  __( "E.G.: https://www.facebook.com/", "idl" ),
                'type' => 'text',
            ),
            array(
                'name' => 'Twitter Profile',
                'id'   => $prefix . 'social_tw',
                'desc' =>  __( "E.G.: https://www.twitter.com/", "idl" ),
                'type' => 'text',
            ),
            array(
                'name' => 'Youtube Page',
                'id'   => $prefix . 'social_yt',
                'desc' =>  __( "E.G.: https://www.youtube.com/", "idl" ),
                'type' => 'text',
            ),
            array(
                'name' => 'Instagram Profile',
                'id'   => $prefix . 'social_ig',
                'desc' =>  __( "E.G.: https://www.instagram.com/", "idl" ),
                'type' => 'text',
            ),
            array(
                'name' => 'Soundcloud Profile',
                'id'   => $prefix . 'social_sc',
                'desc' =>  __( "E.G.: https://www.soundcloud.com/", "idl" ),
                'type' => 'text',
            ),
            array(
                'name'  => __( 'Big Image Parallax', 'idl' ),
                'desc'  => 'Image bigger for background',
                'id'    => $prefix . 'artist_parallax',
                'type'  => 'image_advanced',
            ),
        )
    );

    $meta_boxes[] = array(
        'title'      => __( 'Resources Extra information', 'idl' ),
        'post_types' => array( 'resources' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(

            array(
                'name'  => __( 'Onedrive / Dropbox / Google Drive URL', 'idl' ),
                'desc'  => 'Format: https://onedrive.live.com/about/auth/=y_asdjaskdlj',
                'id'    => $prefix . 'resource_url',
                'type'  => 'text',
            ),
        )
    );
    
     $meta_boxes[] = array(
        'title'      => __( 'Profile Page Extra Info', 'idl' ),
        'post_types' => array( 'artists' ),
        'context'    => 'side',
        'priority'   => 'high',
        'fields' => array(

            array(
                'name'  => __( 'Youtube Video', 'idl' ),
                'desc'  => 'Format: https://www.youtube.com/watch?v=y_asdjaskdlj',
                'id'    => $prefix . 'yvideo',
                'type'  => 'text',
            ),
            array(
                'name'  => __( 'Logo Band', 'idl' ),
                'desc'  => 'Format: A .png prefered',
                'id'    => $prefix . 'logo',
                'type'  => 'image',
            ),
        )
    );

    $meta_boxes[] = array(
        'title'      => __( 'Testimonials Extra Info', 'idl' ),
        'post_types' => array( 'testimonials' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => __( "Artist's Description", 'idl' ),
                'desc'  => 'Position/Role of Artist in band',
                'id'    => $prefix . 'role',
                'type'  => 'text',
            ),
        )
    );

    $meta_boxes[] = array(
        'title'      => __( 'Services Extra Info', 'idl' ),
        'post_types' => array( 'services' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => __( 'Big Image Parallax', 'idl' ),
                'desc'  => 'Image bigger for background',
                'id'    => $prefix . 'service_parallax',
                'type'  => 'image_advanced',
            ),
            array(
                'name'  => __( 'Sidebar Section', 'idl' ),
                'desc'  => 'images on the sidebar',
                'id'    => $prefix . 'services_sidebar',
                'type'  => 'wysiwyg',
            ),
        )
    );

    $meta_boxes[] = array(
        'title'      => __( 'Main Info', 'idl' ),
        'post_types' => array( 'post', 'page', 'services', 'artists' ),
        'context'    => 'side',
        'priority'   => 'high',
        'fields' => array(

            array(
                'name'  => __( 'Title Color', 'idl' ),
                'desc'  => 'Color for title <br/> Will be white if empty',
                'id'    => $prefix . 'tcolor',
                'type'  => 'color',
            ),
        )
    );
    
    


    return $meta_boxes;
}

/* --------------------------------------------------------------
    ADD CUSTOM POST TYPE
-------------------------------------------------------------- */

// Register Custom Post Type
function resources() {

	$labels = array(
		'name'                  => _x( 'Resources', 'Post Type General Name', 'idl' ),
		'singular_name'         => _x( 'Resource', 'Post Type Singular Name', 'idl' ),
		'menu_name'             => __( 'Resources', 'idl' ),
		'name_admin_bar'        => __( 'Resources', 'idl' ),
		'archives'              => __( 'Resources Archives', 'idl' ),
		'attributes'            => __( 'Resource Attributes', 'idl' ),
		'parent_item_colon'     => __( 'Parent Resource:', 'idl' ),
		'all_items'             => __( 'All Resources', 'idl' ),
		'add_new_item'          => __( 'Add New Resource', 'idl' ),
		'add_new'               => __( 'Add New', 'idl' ),
		'new_item'              => __( 'New Resource', 'idl' ),
		'edit_item'             => __( 'Edit Resource', 'idl' ),
		'update_item'           => __( 'Update Resource', 'idl' ),
		'view_item'             => __( 'View Resource', 'idl' ),
		'view_items'            => __( 'View Resources', 'idl' ),
		'search_items'          => __( 'Search Resource', 'idl' ),
		'not_found'             => __( 'Not found', 'idl' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'idl' ),
		'featured_image'        => __( 'Featured Image', 'idl' ),
		'set_featured_image'    => __( 'Set featured image', 'idl' ),
		'remove_featured_image' => __( 'Remove featured image', 'idl' ),
		'use_featured_image'    => __( 'Use as featured image', 'idl' ),
		'insert_into_item'      => __( 'Insert into Resource', 'idl' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Resource', 'idl' ),
		'items_list'            => __( 'Resources list', 'idl' ),
		'items_list_navigation' => __( 'Resources list navigation', 'idl' ),
		'filter_items_list'     => __( 'Filter Resources list', 'idl' ),
	);
	$args = array(
		'label'                 => __( 'Resource', 'idl' ),
		'description'           => __( 'Resources', 'idl' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-editor-kitchensink',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'resources', $args );

}
add_action( 'init', 'resources', 0 );

// Register Custom Post Type
function artists() {

    $labels = array(
        'name'                  => _x( 'Artists', 'Post Type General Name', 'idl' ),
        'singular_name'         => _x( 'Artist', 'Post Type Singular Name', 'idl' ),
        'menu_name'             => __( 'Artists', 'idl' ),
        'name_admin_bar'        => __( 'Artists', 'idl' ),
        'archives'              => __( 'Item Artist', 'idl' ),
        'parent_item_colon'     => __( 'Artist Item:', 'idl' ),
        'all_items'             => __( 'All Artists', 'idl' ),
        'add_new_item'          => __( 'Add New Artist', 'idl' ),
        'add_new'               => __( 'Add Artist', 'idl' ),
        'new_item'              => __( 'New Artist', 'idl' ),
        'edit_item'             => __( 'Edit Artist', 'idl' ),
        'update_item'           => __( 'Update Artist', 'idl' ),
        'view_item'             => __( 'View Artist', 'idl' ),
        'search_items'          => __( 'Search Artist', 'idl' ),
        'not_found'             => __( 'Not found', 'idl' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'idl' ),
        'featured_image'        => __( 'Featured Image', 'idl' ),
        'set_featured_image'    => __( 'Set featured image', 'idl' ),
        'remove_featured_image' => __( 'Remove featured image', 'idl' ),
        'use_featured_image'    => __( 'Use as featured image', 'idl' ),
        'insert_into_item'      => __( 'Insert into Artist', 'idl' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Artist', 'idl' ),
        'items_list'            => __( 'Artists list', 'idl' ),
        'items_list_navigation' => __( 'Artists list navigation', 'idl' ),
        'filter_items_list'     => __( 'Filter Artists list', 'idl' ),
    );
    $args = array(
        'label'                 => __( 'Artist', 'idl' ),
        'description'           => __( 'Lists of Artists on the site', 'idl' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
        'taxonomies'            => array( 'custom_artists' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-groups',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'artists', $args );

}
add_action( 'init', 'artists', 0 );

// Register Custom Post Type
function testimonials() {

    $labels = array(
        'name'                  => _x( 'Testimonials', 'Post Type General Name', 'idl' ),
        'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'idl' ),
        'menu_name'             => __( 'Testimonials', 'idl' ),
        'name_admin_bar'        => __( 'Testimonials', 'idl' ),
        'archives'              => __( 'Testimonials Archive', 'idl' ),
        'parent_item_colon'     => __( 'Parent Testimonial:', 'idl' ),
        'all_items'             => __( 'All Testimonials', 'idl' ),
        'add_new_item'          => __( 'Add New Testimonial', 'idl' ),
        'add_new'               => __( 'Add Testimonial', 'idl' ),
        'new_item'              => __( 'New Testimonial', 'idl' ),
        'edit_item'             => __( 'Edit Testimonial', 'idl' ),
        'update_item'           => __( 'Update Testimonial', 'idl' ),
        'view_item'             => __( 'View Testimonial', 'idl' ),
        'search_items'          => __( 'Search Testimonial', 'idl' ),
        'not_found'             => __( 'Not found', 'idl' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'idl' ),
        'featured_image'        => __( 'Artist Image', 'idl' ),
        'set_featured_image'    => __( 'Set Artist Image', 'idl' ),
        'remove_featured_image' => __( 'Remove Artist Image', 'idl' ),
        'use_featured_image'    => __( 'Use as Artist Image', 'idl' ),
        'insert_into_item'      => __( 'Insert into Testimonial', 'idl' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Testimonial', 'idl' ),
        'items_list'            => __( 'Testimonials list', 'idl' ),
        'items_list_navigation' => __( 'Testimonials list navigation', 'idl' ),
        'filter_items_list'     => __( 'Filter Testimonials list', 'idl' ),
    );
    $args = array(
        'label'                 => __( 'Testimonials', 'idl' ),
        'description'           => __( 'Lists of Testimonials on the site', 'idl' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-universal-access',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'testimonials', $args );

}
add_action( 'init', 'testimonials', 0 );

// Register Custom Post Type
function services() {

    $labels = array(
        'name'                  => _x( 'Services', 'Post Type General Name', 'idl' ),
        'singular_name'         => _x( 'Service', 'Post Type Singular Name', 'idl' ),
        'menu_name'             => __( 'Services', 'idl' ),
        'name_admin_bar'        => __( 'Services', 'idl' ),
        'archives'              => __( 'Services Archive', 'idl' ),
        'parent_item_colon'     => __( 'Parent Service:', 'idl' ),
        'all_items'             => __( 'All Services', 'idl' ),
        'add_new_item'          => __( 'Add New Service', 'idl' ),
        'add_new'               => __( 'Add New', 'idl' ),
        'new_item'              => __( 'New Service', 'idl' ),
        'edit_item'             => __( 'Edit Service', 'idl' ),
        'update_item'           => __( 'Update Service', 'idl' ),
        'view_item'             => __( 'View Service', 'idl' ),
        'search_items'          => __( 'Search Service', 'idl' ),
        'not_found'             => __( 'Not found', 'idl' ),
        'not_found_in_trash'    => __( 'Not found in trash', 'idl' ),
        'featured_image'        => __( 'Featured Image', 'idl' ),
        'set_featured_image'    => __( 'Put Featured Image', 'idl' ),
        'remove_featured_image' => __( 'Remove Featured Image', 'idl' ),
        'use_featured_image'    => __( 'Use as Featured Image', 'idl' ),
        'insert_into_item'      => __( 'Insert into Service', 'idl' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Service', 'idl' ),
        'items_list'            => __( 'Services List', 'idl' ),
        'items_list_navigation' => __( 'Services lists nvegtion', 'idl' ),
        'filter_items_list'     => __( 'Services Filter List', 'idl' ),
    );
    $args = array(
        'label'                 => __( 'Service', 'idl' ),
        'description'           => __( 'Services from company', 'idl' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-image-filter',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'services', $args );

}
add_action( 'init', 'services', 0 );

// Register Custom Post Type
function faqs() {

    $labels = array(
        'name'                  => _x( 'Questions', 'Post Type General Name', 'idl' ),
        'singular_name'         => _x( 'Question', 'Post Type Singular Name', 'idl' ),
        'menu_name'             => __( 'FAQ', 'idl' ),
        'name_admin_bar'        => __( 'FAQ', 'idl' ),
        'archives'              => __( 'FAQ Archive', 'idl' ),
        'parent_item_colon'     => __( 'Parent Question:', 'idl' ),
        'all_items'             => __( 'All Questions', 'idl' ),
        'add_new_item'          => __( 'Add New Question', 'idl' ),
        'add_new'               => __( 'Add New', 'idl' ),
        'new_item'              => __( 'New Question', 'idl' ),
        'edit_item'             => __( 'Edit Question', 'idl' ),
        'update_item'           => __( 'Update Question', 'idl' ),
        'view_item'             => __( 'View Question', 'idl' ),
        'search_items'          => __( 'Search Question', 'idl' ),
        'not_found'             => __( 'Not Found', 'idl' ),
        'not_found_in_trash'    => __( 'Not Found in Trash', 'idl' ),
        'featured_image'        => __( 'Featured Imge', 'idl' ),
        'set_featured_image'    => __( 'Set Featured Image', 'idl' ),
        'remove_featured_image' => __( 'Remove Featured Image', 'idl' ),
        'use_featured_image'    => __( 'Use as Featured Image', 'idl' ),
        'insert_into_item'      => __( 'Insert into Question', 'idl' ),
        'uploaded_to_this_item' => __( 'Uploaded to this Question', 'idl' ),
        'items_list'            => __( 'Questions List', 'idl' ),
        'items_list_navigation' => __( 'Questions List Navigation', 'idl' ),
        'filter_items_list'     => __( 'Filter Questions List', 'idl' ),
    );
    $args = array(
        'label'                 => __( 'Question', 'idl' ),
        'description'           => __( 'Frequently Questions Asked', 'idl' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-format-status',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'faqs', $args );

}
add_action( 'init', 'faqs', 0 );


/* --------------------------------------------------------------
    ADD CUSTOM IMAGE SIZE
-------------------------------------------------------------- */
if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 9999, 400, true);
}
if ( function_exists( 'add_image_size' ) ) {
    add_image_size('avatar', 100, 100, true);
    add_image_size('blog_img', 400, 300, true);
    add_image_size('single_img', 636, 297, true);
}


/* --------------------------------------------------------------
    ADD CUSTOM WALKER BOOTSTRAP
-------------------------------------------------------------- */

// WALKER COMPLETO TOMADO DESDE EL NAVBAR COLLAPSE
require_once('inc/wp_bootstrap_navwalker.php');

// WALKER CUSTOM SI DEBO COLOCAR ICONOS AL LADO DEL MENU PRINCIPAL - SU ESTRUCTURA ESTA DENTRO DEL MISMO ARCHIVO
require_once('inc/wp_walker_custom.php');

/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

require_once('inc/wp_custom_functions.php');

function sum_video_parser($url, $autoplay, $wdth=320, $hth=320){
    $iframe = '';
    $thumb_link = '';
    if (isset($autoplay)){
        if ($autoplay == true){
            $auto = '?autoplay=0';
        } else {
            $auto = '?autoplay=0';
        }
    }
    if (strpos($url, 'youtube.com') !== FALSE) {
        $step1=explode('v=', $url);
        $step2 =explode('&amp;',$step1[1]);
        if (strlen($step2[0]) > 12){
            $step3 = substr($step2[0], 0, 11);
        } else {
            $step3 = $step2[0];
        }
        $iframe ='<iframe class="embed-responsive-item" width="'.$wdth.'" height="'.$hth.'" src="http://www.youtube.com/embed/'.$step2[0].$auto.'&showinfo=0&modestbranding=1&rel=0" frameborder="0"></iframe>';
        $embed ='<object class="embed-responsive-item" width="'.$wdth.'" height="'.$hth.'" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0"><param name="src" value="http://www.youtube.com/v/'.$step2[0].'&'.$auto.'" /><param name="wmode" value="transparent" /><param name="embed" value="" /><embed width="'.$wdth.'" height="'.$hth.'" type="application/x-shockwave-flash" src="http://www.youtube.com/v/'.$step2[0].$auto.'" wmode="transparent" embed="" /></object>';
        $thumbnail_str = 'http://img.youtube.com/vi/'.$step3.'/2.jpg';
        $fullsize_str = 'http://img.youtube.com/vi/'.$step3.'/0.jpg';
        $thumb_link = htmlentities($fullsize_str);

    }
    else if (strpos($url, 'vimeo') !== FALSE) {

        $id=str_replace('http://vimeo.com/','',$url);
        $embedurl = "http://player.vimeo.com/video/".$id;

        $iframe = '<iframe class="embed-responsive-item" src="'.$embedurl.'"
 width="'.$wdth.'" height="'.$hth.'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen>
</iframe>';
        $embed ='
<embed width="'.$wdth.'" height="'.$hth.'" type="application/x-shockwave-flash"
src="'.$embedurl.'"  />
';
        $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$id.php"));
        if (!empty($hash) && is_array($hash)) {
            //$video_str = 'http://vimeo.com/moogaloop.swf?clip_id=%s';
            $thumbnail_str = $hash[0]['thumbnail_small'];
            $fullsize_str = '<img src="'.$hash[0]['thumbnail_large'].'" />';

        }

    }


    return array("embed"=>$iframe,"thumb_image" =>$thumb_link);

}

add_filter('widget_text', 'do_shortcode');





?>
