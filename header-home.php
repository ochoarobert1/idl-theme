<!DOCTYPE html>
<html <?php language_attributes() ?>>
    <head>
        <meta charset="<?php bloginfo('charset') ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link href="//www.google-analytics.com" rel="dns-prefetch" />
        <link href="<?php echo esc_url(get_template_directory_uri()); ?>/favicon.ico" rel="shortcut icon" />
        <meta name="robot" content="NOODP, INDEX, FOLLOW" />
        <meta name="author" content="IDL Entertaiment" />
        <meta name="language" content="VE" />
        <meta name="geo.position" content="-32.5227018,-55.8008545" />
        <meta name="ICBM" content="-32.5227018,-55.8008545" />
        <meta name="geo.region" content="VE" />
        <meta name="geo.placename" content="Uruguay" />
        <meta name="DC.title" content="<?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?>" />
        <title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
        <meta name="title" content="<?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?>">
        <?php $current_url = $_SERVER['REQUEST_URI']; $clean_url = str_replace('/', '', $current_url); ?>
        <?php if(is_single()) : $the_slug = $clean_url; $args=array('name' => $the_slug, 'posts_per_page' => 1); $my_posts = get_posts( $args ); ?>
        <?php if( $my_posts ) { $excerpt = $my_posts[0]->post_excerpt; echo '<meta name="description" content="' . htmlentities($excerpt, ENT_QUOTES, 'UTF-8') . '" />'; } ?>
        <?php endif; ?>
        <?php if (is_home() ) { echo '<meta name="description" content="'. get_bloginfo("name") .' | ' . get_bloginfo("description") . '">'; } ?>
        <?php if (is_page() ) { echo '<meta name="description" content="'. get_bloginfo("name") .' | ' . get_bloginfo("description") . '">'; } ?>
        <?php if(is_single()) : $the_slug = $clean_url; $args=array('name' => $the_slug, 'posts_per_page' => 1); $my_posts = get_posts( $args ); ?>
        <?php if( $my_posts ) { $terms = get_the_terms( $my_posts[0]->ID, 'post_tag' ); if ( $terms && ! is_wp_error( $terms ) ) : $draught_links = array(); ?>
        <?php foreach ( $terms as $term ) { $draught_links[] = $term->name; } $on_draught = join( ", ", $draught_links ); endif;
                               echo '<meta name="keywords" content="KEYWORDS , '. $on_draught .'" />'; } ?>
        <?php endif; ?>
        <?php if (is_home() || is_page() ) { echo '<meta name="keywords" content="KEYWORDS" />'; } ?>
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@idlent" />
        <meta name="twitter:creator" content="@idlent" />
        <meta property='fb:admins' content='100000133943608' />
        <meta property="fb:app_id" content="" />
        <meta property="og:title" content="<?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?>" />
        <meta property="og:site_name" content="IDL Entertaiment" />
        <meta property="og:type" content="article" />
        <meta property="og:locale" content="es_ES" />
        <meta property="og:url" content="<?php if(is_single()) { the_permalink(); } else { echo 'http://idlentertaiment.com/'; }?>" />
        <?php if(is_single()) : $the_slug = $clean_url; $args=array('name' => $the_slug, 'posts_per_page' => 1); $my_posts = get_posts( $args ); ?>
        <?php if( $my_posts ) { $excerpt = $my_posts[0]->post_excerpt; echo '<meta property="og:description" content="' . htmlentities($excerpt, ENT_QUOTES, 'UTF-8') . '" />'; } ?>
        <?php endif; ?>
        <?php if (is_home() ) { echo '<meta property="og:description" content="'. get_bloginfo("name") .'" | "' . get_bloginfo("description") . '">'; } ?>
        <?php if (is_page() ) { echo '<meta property="og:description"  content="'. get_bloginfo("name") .'" | "' . get_bloginfo("description") . '">'; } ?>
        <meta property="og:image" content='<?php if(is_single()){ $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); echo $url; } else { echo esc_url( get_template_directory_uri() ) ."/images/oglogo.png"; } ?>' />
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php wp_head() ?>
        <!--[if IE]> <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script> <![endif]-->
        <!--[if IE]>  <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script> <![endif]-->
        <?php get_template_part('inc/fb-script'); ?>
        <?php get_template_part('inc/ga-script'); ?>
        <meta name="google-site-verification" content="PfFvFEA4w5VwiY8CqGWsScgybYFPIt2ja8CZ2u-kik4" />
    </head>
    <body <?php body_class() ?>>
        <div id="fb-root"></div>
        <header class="container-fluid">
            <div class="row">
                <div id="sticker" class="the-header col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                <div class="col-md-1 col-sm-2 col-xs-2 no-paddingl no-paddingr">
                                    <a href="<?php echo home_url('/'); ?>"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="IDL" class="img-brands" /></a>
                                </div>
                                <div class="col-md-3 col-sm-2 col-xs-3">
                                    <div class="header-slogan">Your Next <br />Experience</div>
                                </div>
                                <div class="col-md-7 col-sm-8 col-xs-7 no-paddingl">
                                    <div class="header-social-icons col-md-12 col-sm-12 col-xs-12">
                                        <a href="https://www.facebook.com/IDLEntertainment/" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/fb.png" alt=""></a>
                                        <a href="https://twitter.com/idlent" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/tw.png" alt=""></a>
                                        <a href="https://www.instagram.com/idlentertainment/" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ig.png" alt=""></a>
                                        <a href="https://soundcloud.com/idlentertainment" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/sc.png" alt=""></a>
                                        <a href="https://youtube.com/idlentertainment" target="_blank"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/yt.png" alt=""></a>

                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                        <nav class="navbar navbar-default">
                                            <div class="container-fluid">
                                                <!-- Brand and toggle get grouped for better mobile display -->
                                                <div class="navbar-header">
                                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                                        <span class="sr-only">Toggle navigation</span>
                                                        <span class="icon-bar"></span>
                                                        <span class="icon-bar"></span>
                                                        <span class="icon-bar"></span>
                                                    </button>
                                                    <a class="navbar-brand" href="#">

                                                    </a>
                                                </div>

                                                <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">
                                                    <ul id="menu-main-menu" class="nav navbar-nav navbar-right">
                                                        <li id="menu-item-46" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-19 current_page_item menu-item-46 active"><a title="Home" href="<?php echo home_url('/'); ?>">Home</a></li>
                                                        <li id="menu-item-48" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-48"><a data-scroll title="About" href="#about">About</a></li>
                                                        <li id="menu-item-55" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-55"><a data-scroll title="News" href="#news">News</a></li>
                                                        <li id="menu-item-58" class="menu-item menu-item-type-post_type_archive menu-item-object-artists menu-item-58"><a title="Artists" href="<?php echo home_url('/artists'); ?>">Artists</a></li>
                                                        <li id="menu-item-57" class="menu-item menu-item-type-post_type_archive menu-item-object-services menu-item-57"><a data-scroll title="Services" href="#services">Services</a></li>
                                                        <li id="menu-item-47" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-47"><a title="Contact" href="<?php echo home_url('/contact'); ?>">Contact</a></li>
                                                    </ul></div>
                                            </div><!-- /.container-fluid -->
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
