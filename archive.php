<?php get_header(); ?>
<main class="container-fluid" role="main">
    <div class="row">
        <section class="big-hero col-md-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1><?php single_cat_title(); ?></h1>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <section class="page-content col-md-12 no-paddingl no-paddingr">
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <?php $defaultatts = array('class' => 'img-responsive'); ?>
                        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                        <article id="post-<?php the_ID(); ?>" class="archive-item col-md-3 no-paddingl no-paddingr <?php echo join(' ', get_post_class()); ?>">


                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h2><?php the_title(); ?></h2></a>
                            <p><?php the_excerpt(); ?></p>
                            <?php edit_post_link(); ?>

                            <div class="clearfix"></div>
                            <hr>
                        </article>
                        <?php endwhile; ?>
                        <div class="pagination col-md-12">
                            <?php if(function_exists('wp_paginate')) { wp_paginate(); } else { posts_nav_link(); } ?>
                        </div>
                        <?php endif; ?>
                    </article>
                </section>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
