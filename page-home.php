<?php get_header('home'); ?>
<?php the_post(); ?>
<?php $defaultatts = array('class' => 'img-responsive');  ?>
<div class="giphy-overlay giphy-show">
    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/giphy.gif" alt="Logo" class="img-responsive" />
</div>
<div class="video-overlay overlay-hide">
    <a id="closevideo">X</a>
    <video controls loop class="fullVideo">
        <source src="<?php echo esc_url(get_template_directory_uri()); ?>/video/long.mp4" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
        <source src="<?php echo esc_url(get_template_directory_uri()); ?>/video/long.webm" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.
    </video>
</div>
<main class="container-fluid" role="main">
    <div class="row">
        <section class="the-video col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="canvas-container">
                <div class="canvas-img-container canvas-img-container-left">
                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/canvas.png" alt="" class="canvas-img" />
                </div>
            </div>
            <video autoplay loop class="fillWidth">
                <source src="<?php echo esc_url(get_template_directory_uri()); ?>/video/short.mp4" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.
                <source src="<?php echo esc_url(get_template_directory_uri()); ?>/video/short.webm" type="video/webm" />Your browser does not support the video tag. I suggest you upgrade your browser.
            </video>
        </section>
        <section id="news" class="the-promo col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <?php $args = array('posts_per_page' => 1, 'meta_query' => array( array( 'key' => 'rw_sticker', 'value' => array(1), 'compare' => 'IN' ))); ?>
                    <?php query_posts($args); ?>
                    <?php while (have_posts()) : the_post(); ?>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php the_post_thumbnail('full', $defaultatts); ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <h2><?php the_title(); ?></h2>
                        <?php the_content(); ?>
                        <a href="<?php echo home_url('/news'); ?>"><button class="btn btn-md btn-default">Read All News</button></a>
                    </div>
                    <?php endwhile; wp_reset_query(); ?>
                </div>
            </div>
        </section>
        <section id="about" class="the-about col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="about-content col-md-12 col-sm-12 col-xs-12">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-big.jpg" alt="IDL Entertaiment" class="img-responsive" />
                        <?php the_content(); ?>
                        <a href="<?php echo home_url('/about'); ?>"><button class="btn btn-md btn-default">Read More</button></a>
                    </div>
                    <div class="col-md-2 col-md-offset-5  col-sm-2 col-sm-offset-2 col-xs-4 col-xs-offset-4">
                        <hr>
                    </div>
                </div>
            </div>
        </section>
        <div id="services" class="the-services col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <h2>Services we Provide</h2>

                    </div>
                </div>
            </div>
            <?php $args = array('post_type' => 'services'); ?>
            <?php query_posts($args); ?>
            <?php $i = 1; ?>
            <?php $y = 1; ?>
            <?php while (have_posts()) : the_post(); ?>
            <a href="<?php the_permalink(); ?>">
                <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                <div class="front-service-item col-md-3 col-sm-6 col-xs-12 no-paddingl no-paddingr wow fadeIn delay-<?php echo $i; ?>" style="background: url(<?php echo $url; ?>)">
                    <div class="front-service-item-mask"></div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?php the_title(); ?>
                    </div>
                </div>
            </a>
            <?php $i++; $y++; if ($i == 4) { $i = 1; }endwhile; ?>
            <?php wp_reset_query();?>
        </div>
    </div>
</main>
<?php get_footer(); ?>
