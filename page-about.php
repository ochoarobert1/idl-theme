<?php get_header(); ?>
<?php the_post(); ?>
<?php $defaultatts = array('class' => 'img-responsive'); ?>
<main class="container-fluid" role="main">
    <div class="row">
        <?php $files = rwmb_meta( 'rw_page_parallax', 'type=image&size=full' ); // Prior to 4.8.0
        if ( !empty( $files ) ) {
            foreach ( $files as $file ) {
                $url = $file['url'];
            }
        } ?>
        <section class="big-hero col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr" <?php if (!empty($files)){ echo 'style="background: url(' . $url . ');"'; } ?> >
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?php $pickcolor = get_post_meta(get_the_ID(), 'rw_tcolor', true); ?>
                        <?php if ($pickcolor == ''){ $color = '#FFF'; } else { $color = $pickcolor; } ?>
                        <h1 style="color: <?php echo $color; ?>"><?php the_title(); ?></h1>
                        <p style="color: <?php echo $color; ?>"><?php echo get_post_meta(get_the_ID(), 'rw_p_title', true);?></p>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <section class="page-content col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                    <div class="col-md-8 col-sm-12 col-xs-12">
                        <article id="post-<?php echo get_the_ID(); ?>" <?php post_class(); ?>>
                            <?php the_content(); ?>
                        </article>
                    </div>
                    <div class="col-md-4 visible-md visible-lg">

                        <?php $images = rwmb_meta( 'rw_img_sidebar', 'size=full' );      // Prior to 4.8.0
                        if ( !empty( $images ) ) {
                            foreach ( $images as $file ) { ?>
                            <?php var_dump($file); ?>
                        <div class="about-img col-md-12">
                            <img src="<?php echo $file['url']; ?>" alt="" class="img-responsive" />
                        </div>
                        <?php }
                        } ?>
                    </div>
                    <div class="clearfix"></div>

                </section>
            </div>
        </div>
        <div class="testimonials-container col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <h2 class="section-title">Testimonials</h2>
                        <?php $args = array('post_type' => 'testimonials', 'posts_per_page' => 3); ?>
                        <?php query_posts($args); ?>
                        <?php while (have_posts()) : the_post(); ?>
                        <div class="testimonials-item wow fadeInDown col-md-4 col-sm-12 col-xs-12">
                            <div class="testimonials-item-img col-md-12 col-sm-12 col-xs-12">
                                <?php the_post_thumbnail('full', $defaultatts); ?>
                            </div>
                            <div class="testimonials-content col-md-12 col-sm-12 col-xs-12">
                                <?php the_content(); ?>
                            </div>
                            <div class="testimonials-author col-md-12 col-sm-12 col-xs-12">
                                <h2><?php the_title(); ?></h2>
                                <p><?php echo get_post_meta(get_the_ID(), 'rw_role', true); ?></p>
                            </div>
                        </div>
                        <?php endwhile; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
